# README #

### Changes ot the IWorker Interface ###

The IWorker interface has been broken into two interfaces, WorkInterface and EatInterface. The WorkInterface interface has only work() and the EatInterface interface has only eat() as their methods. The reasoning and principles are as follows: 

* Interface Segregation: The robot class does not take breaks, so the eat() method inherited from the IWorker interface would be redundant for this class. This would violate the Interface Segregation Principle, as the robot class would depend on an interface it does not use. Therefore, the IWorker interface should be broken into two interfaces, one for working classes and one for eating classes. Now the robot class can implement an interface for working without needing the eat() method.

* Single Responsibility: The former IWorker interface had two responsibilities, working and taking breaks, which were two unrelated responsibilities. This was the reason that the robot class could not implement the IWorker interface without redundant code. Breaking IWorker into two interfaces satisfies the Single Responsibility principle.

-Tanner C.-
The Manager class has been changed to instead use "WorkInterface" instead of the previous "IWorker" 

* Single Responsiblity: The manager class only has one responsibility and that's to manage the classes that implement "WorkInterface".

Brice Ashburn:

I worked on the Worker class and this class made me think of Open/Close because we added to the class using Worker and Eat interfaces.

// Author Eric Figueroa
I handled the SuperWorker class and when reorganizing my section of the code by updating the Worker interface into two interfaces of WorkInterface and EatInterface.

* Open Closed principle in this case the software components should be open for extension, but closed for modification. 

Erik Cortez:

I did the Robot class which used the Open/Close principle since it is open to changes by the work interface but closed to modification 


### Changes to the Manager Class ###

We put the Manager class in its own folder inside the src folder. The reason the manager class is its own package and folder is to minimize the interface for the ThreeSolidMain class in the main file. This promotes the interface segregation principle. The main class doe snot need to know anything about the threesolid classes. It does not depend on classes and methods that it does not use. All the main class does is import the manager and not the threesolid package.


### How to run this program ###

To run this program, navigate to the project directory and use the following ant commands in the specified order:

* ant clean

* ant compile

* ant jar

* ant run






