// Author Eric Figueroa
package threesolid;

public class SuperWorker implements WorkInterface, EatInterface {
	public void work() {
		//.... working much more
	}

	public void eat() {
		//.... eating in launch break
	}
}
