/*

Author: Brice Ashburn

*/

package threesolid;

public class Worker implements EatInterface, WorkInterface
{
	public void work() 
	{
		// ....working
	}

	public void eat()
	{
		//.... eating in launch break
	}
}
