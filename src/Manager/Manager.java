package Manager;
import threesolid.WorkInterface;

/*
 * 	Author: Tanner Coker
 */

public class Manager 
{
	WorkInterface worker;

	public void Manager() {

	}
	
	public void setWorker(WorkInterface w) {
		worker=w;
	}

	public void manage() {
		worker.work();
	}
	
}
